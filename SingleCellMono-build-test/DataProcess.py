################################################################################
"""
This script takes CSV files for the individual threads from the GEANT4
simulation, and combines them into CSV files for each run. It also performs
basic maths on the raw data to print, either to stdout or, in batch mode, to
an output file that has been specified on the command line.

The quantities printed, for the threads taken as columns, are the mean of means,
the standard deviation of means, the mean of counts, the standard deviation
of counts, the mean of the total energy deposited for the run, and the standard
deviation of the total energy deposited in the run.
"""

import numpy as np
import pandas as pd
import argparse

################################################################################
# Take command line arguments for the number of threads used the simulation
# (default = 12), the folder being operated in (default = ./), and the label,
# usually a physical parameter such as photon energy (default ="")
parser = argparse.ArgumentParser()

parser.add_argument("-j", metavar = "Number of threads", default = 12)

parser.add_argument("-f", metavar="Folder to operate in", default = "./")

parser.add_argument("-l", metavar="File label", default = "")

args = parser.parse_args()
threads = int(args.j)
label = args.l
folder = args.f

################################################################################
# Create a dataframe to hold all the thread results for a run in one place
Edep = pd.DataFrame()

for i in range (threads):
  
    f1 = folder + "radioprotection_nt_102_t" + str(i) + ".csv"
    Edep = pd.concat([Edep, pd.read_csv(f1, header=None, skiprows=5)],axis=1)
    

Edep.columns = ["thread " + str(i) for i in range(threads)]

################################################################################
# Dome some summary maths, averaging over the threads used, and print it
# Edep per particle
edep_per = Edep.mean().mean()
edep_per_err = Edep.mean().std()
# Number of hits
count = Edep.count().mean()
count_err = Edep.count().std()
# Edep per thread (which has an equal fraction of the total incident)
edep_tot = edep_per * count
edep_tot_err = edep_tot*np.sqrt((edep_per_err/edep_per)**2 + 
                                                        (count_err/count)**2)

################################################################################
# Print summary numbers and save raw file
print(label, edep_per, edep_per_err, count, count_err, edep_tot, edep_tot_err)

Edep.to_csv(folder+"Edep_"+label+".csv")